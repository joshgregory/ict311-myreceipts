package com.bignerdranch.android.myreceipts;

import android.support.v4.app.Fragment;

/**
 * Created by test1 on 10/10/2018.
 */

public class ReceiptListActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new ReceiptListFragment();
    }
}
